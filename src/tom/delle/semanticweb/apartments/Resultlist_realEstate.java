
package tom.delle.semanticweb.apartments;


import com.google.gson.annotations.SerializedName;

public class Resultlist_realEstate{
   	
	@SerializedName("@id")
	private String id;
	@SerializedName("@xsi.type")
   	private String xsi_type;
   	private Address address;
   	private ApiSearchData apiSearchData;
   	private String balcony;
   	private String builtInKitchen;
   	private String companyWideCustomerId;
   	private Courtage courtage;
   	private String floorplan;
   	private String garden;
   	private String listingType;
   	private Number livingSpace;
   	private Number numberOfRooms;
   	private Price price;
   	private String privateOffer;
   	private String streamingVideo;
   	private String title;

 	public String getid(){
		return this.id;
	}
	public void setid(String id){
		this.id = id;
	}
 	public String getxsi_type(){
		return this.xsi_type;
	}
	public void setxsi_type(String xsi_type){
		this.xsi_type = xsi_type;
	}
 	public Address getAddress(){
		return this.address;
	}
	public void setAddress(Address address){
		this.address = address;
	}
 	public ApiSearchData getApiSearchData(){
		return this.apiSearchData;
	}
	public void setApiSearchData(ApiSearchData apiSearchData){
		this.apiSearchData = apiSearchData;
	}
 	public String getBalcony(){
		return this.balcony;
	}
	public void setBalcony(String balcony){
		this.balcony = balcony;
	}
 	public String getBuiltInKitchen(){
		return this.builtInKitchen;
	}
	public void setBuiltInKitchen(String builtInKitchen){
		this.builtInKitchen = builtInKitchen;
	}
 	public String getCompanyWideCustomerId(){
		return this.companyWideCustomerId;
	}
	public void setCompanyWideCustomerId(String companyWideCustomerId){
		this.companyWideCustomerId = companyWideCustomerId;
	}
 	public Courtage getCourtage(){
		return this.courtage;
	}
	public void setCourtage(Courtage courtage){
		this.courtage = courtage;
	}
 	public String getFloorplan(){
		return this.floorplan;
	}
	public void setFloorplan(String floorplan){
		this.floorplan = floorplan;
	}
 	public String getGarden(){
		return this.garden;
	}
	public void setGarden(String garden){
		this.garden = garden;
	}
 	public String getListingType(){
		return this.listingType;
	}
	public void setListingType(String listingType){
		this.listingType = listingType;
	}
 	public Number getLivingSpace(){
		return this.livingSpace;
	}
	public void setLivingSpace(Number livingSpace){
		this.livingSpace = livingSpace;
	}
 	public Number getNumberOfRooms(){
		return this.numberOfRooms;
	}
	public void setNumberOfRooms(Number numberOfRooms){
		this.numberOfRooms = numberOfRooms;
	}
 	public Price getPrice(){
		return this.price;
	}
	public void setPrice(Price price){
		this.price = price;
	}
 	public String getPrivateOffer(){
		return this.privateOffer;
	}
	public void setPrivateOffer(String privateOffer){
		this.privateOffer = privateOffer;
	}
 	public String getStreamingVideo(){
		return this.streamingVideo;
	}
	public void setStreamingVideo(String streamingVideo){
		this.streamingVideo = streamingVideo;
	}
 	public String getTitle(){
		return this.title;
	}
	public void setTitle(String title){
		this.title = title;
	}
}
