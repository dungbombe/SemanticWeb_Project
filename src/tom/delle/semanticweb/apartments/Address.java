
package tom.delle.semanticweb.apartments;


public class Address{
   	private String city;
   	private String houseNumber;
   	private String postcode;
   	private String preciseHouseNumber;
   	private String quarter;
   	private String street;
   	private Wgs84Coordinate wgs84Coordinate;

 	public String getCity(){
		return this.city;
	}
	public void setCity(String city){
		this.city = city;
	}
 	public String getHouseNumber(){
		return this.houseNumber;
	}
	public void setHouseNumber(String houseNumber){
		this.houseNumber = houseNumber;
	}
 	public String getPostcode(){
		return this.postcode;
	}
	public void setPostcode(String postcode){
		this.postcode = postcode;
	}
 	public String getPreciseHouseNumber(){
		return this.preciseHouseNumber;
	}
	public void setPreciseHouseNumber(String preciseHouseNumber){
		this.preciseHouseNumber = preciseHouseNumber;
	}
 	public String getQuarter(){
		return this.quarter;
	}
	public void setQuarter(String quarter){
		this.quarter = quarter;
	}
 	public String getStreet(){
		return this.street;
	}
	public void setStreet(String street){
		this.street = street;
	}
 	public Wgs84Coordinate getWgs84Coordinate(){
 		if(this.wgs84Coordinate==null)return null;
 		else
		return this.wgs84Coordinate;
	}
	public void setWgs84Coordinate(Wgs84Coordinate wgs84Coordinate){
		this.wgs84Coordinate = wgs84Coordinate;
	}
}
