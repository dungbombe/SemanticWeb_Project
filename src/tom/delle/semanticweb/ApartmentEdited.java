package tom.delle.semanticweb;

import java.util.ArrayList;

public class ApartmentEdited {

	private String price, size, longitude, latitude, link, priceSqrm, priceBelowAverage,id;
	private ArrayList<DistanceObject> listOfParks;
	private ArrayList<DistanceObject> listOfSchools;
	
	public ApartmentEdited(
			String price,
			String size,
			String longitude,
			String latitude,
			String link,
			String priceSqrm,
			String priceBelowAverage,
			ArrayList<DistanceObject> parklist,
			ArrayList<DistanceObject> schoollist,
			String id
			){
		this.price = price;
		this.size = size;
		this.longitude = longitude;
		this.latitude = latitude;
		this.link = link;
		this.priceBelowAverage = priceBelowAverage;
		this.priceSqrm = priceSqrm;
		this.listOfParks = parklist;
		this.listOfSchools = schoollist;
		this.id = id;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getPriceSqrm() {
		return priceSqrm;
	}

	public void setPriceSqrm(String priceSqrm) {
		this.priceSqrm = priceSqrm;
	}

	public ArrayList<DistanceObject> getListOfParks() {
		return listOfParks;
	}

	public void setListOfParks(ArrayList<DistanceObject> listOfParks) {
		this.listOfParks = listOfParks;
	}

	public ArrayList<DistanceObject> getListOfSchools() {
		return listOfSchools;
	}

	public void setListOfSchools(ArrayList<DistanceObject> listOfSchools) {
		this.listOfSchools = listOfSchools;
	}

	public String getPriceBelowAverage() {
		return priceBelowAverage;
	}

	public void setPriceBelowAverage(String priceBelowAverage) {
		this.priceBelowAverage = priceBelowAverage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
	public void printAll(){
		System.out.println("");
		System.out.println(this.id+"\n"+this.link+"\n"+this.size+"\n"+this.price+"\n"+this.priceSqrm+"\n"+this.priceBelowAverage+"\n"+this.latitude+"\n"+this.longitude);
		DistanceObject dc;
		for(int i = 0; i<this.getListOfParks().size();i++){
			dc = this.getListOfParks().get(i);
			System.out.println(dc.getValue());
			
			
			
		}
		for(int i = 0; i<this.getListOfSchools().size();i++){
			dc = this.getListOfSchools().get(i);
			System.out.println(dc.getValue());
			
			
			
		}
		
		
		
		
		
	}
	
	
}
