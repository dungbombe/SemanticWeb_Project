
package tom.delle.semanticweb.schools;


public class Long{
   	private String datatype;
   	private String type;
   	private String value;

 	public String getDatatype(){
		return this.datatype;
	}
	public void setDatatype(String datatype){
		this.datatype = datatype;
	}
 	public String getType(){
		return this.type;
	}
	public void setType(String type){
		this.type = type;
	}
 	public String getValue(){
 		if(this.value==null)return null;
		return this.value;
	}
	public void setValue(String value){
		this.value = value;
	}
}
