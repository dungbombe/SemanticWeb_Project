package tom.delle.semanticweb;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import javax.xml.validation.SchemaFactoryLoader;

import com.hp.hpl.jena.ontology.DatatypeProperty;
import com.hp.hpl.jena.ontology.Individual;
import com.hp.hpl.jena.ontology.OntClass;
import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.rdf.model.Bag;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;
import com.hp.hpl.jena.sparql.vocabulary.FOAF;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;


public class Main {

	public static void main(String[] args) throws IOException {
	
		String sURL_apartments = "https://gitlab.com/dungbombe/SemanticWeb_Project/raw/master/apartment.json"; 
		String sURL_schools = "https://gitlab.com/dungbombe/SemanticWeb_Project/raw/master/schools.json";
		String sURL_parks = "https://gitlab.com/dungbombe/SemanticWeb_Project/raw/master/parks.json";
		String sURL_price = "http://www.wohnungsboerse.net/immobilienpreise-Leipzig/7390";
		String baseURI = "http://tom.delle.sw/";
		ArrayList<ApartmentEdited> finalApartments;
		ArrayList<ParkEdited> finalParks;
		ArrayList<SchoolEdited> finalSchools;
		
	DataCollector dc = new DataCollector();
	dc.getApartments(sURL_apartments);
	dc.getSchools(sURL_schools);
	
	
	
	
	dc.getAveragePrice(sURL_price);
	dc.getParks(sURL_parks);
	finalParks = dc.getParksEdited();
	System.out.println(finalParks.size()+"Parkgröße");
	finalSchools = dc.getSchoolsEdited();	
	finalApartments = dc.getApartmentsEdited();
	   
//		finalApartments.get(0).printAll();
//		finalApartments.get(1).printAll();
//		finalApartments.get(2).printAll();
//		finalApartments.get(3).printAll();
//		finalApartments.get(4).printAll();
//	
	
	
//	// create an empty Model
//	Model model = ModelFactory.createDefaultModel();
//	model.add("", RDF.type, RDFS.Class);
//	
	
	
//	Resource apartmentResource;
//	ApartmentEdited actualApartment;
//	String hasID, hasSize, hasPrice, hasBelowAverage, hasPriceSqrt, hasLink, hasLatitude, hasLongitude;
//	
	
//	for(int i = 0; i< finalApartments.size();i++){
//		
//	actualApartment = finalApartments.get(i);
//	hasID = actualApartment.getId();
//	hasPrice = actualApartment.getPrice();
//	hasSize = actualApartment.getSize();
//	hasPriceSqrt = actualApartment.getPriceSqrm();
//	hasBelowAverage = actualApartment.getPriceBelowAverage();
//	hasLink = actualApartment.getLink();
//	hasLatitude = actualApartment.getLatitude();
//	hasLongitude = actualApartment.getLongitude();
//	ArrayList<DistanceObject> parkDistances = actualApartment.getListOfParks();
//	ArrayList<DistanceObject> schoolDistances = actualApartment.getListOfSchools();
//	
//	apartmentResource = model.createResource(baseURI+hasID);
//				
///*
// * 
// * Create Apartment Resource and basic Properties
// * 
// * */
//	
//	apartmentResource.addProperty(ResourceFactory.createProperty(baseURI+hasID,"hasID"), hasID)
//	.addProperty(ResourceFactory.createProperty(baseURI,"hasLink"), hasLink)
//	.addProperty(ResourceFactory.createProperty(baseURI,"hasSize"), hasSize) 
//	.addProperty(ResourceFactory.createProperty(baseURI,"hasPriceSqrt"), hasPriceSqrt) 
//	.addProperty(ResourceFactory.createProperty(baseURI,"hasBelowAverage"), hasBelowAverage) 		
//	.addProperty(ResourceFactory.createProperty(baseURI,"hasLatitude"), hasLatitude) 			
//	.addProperty(ResourceFactory.createProperty(baseURI,"hasLongitude"), hasLongitude);
//
//	/*
//	 * 
//	 * Create ParkDistance/SchoolDistanced Resource and add Park Properties
//	 * 
//	 * */	
//	
//
//	
//	
//	
//				
//	}
	
	
//		model.setNsPrefix("ap", baseURI);
//		model.setNsPrefix("xsd", "http://www.w3.org/2001/XMLSchema#");
//
//		model.write(System.out, "RDF/XML");
//		
		
		
	
	// Ontology
	 OntModel ontModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM_RULE_INF);
	 OntClass apartmentClass = ontModel.createClass(baseURI+"apartment");
	 OntClass schoolClass = ontModel.createClass(baseURI+"school");
	 OntClass parkClass = ontModel.createClass(baseURI+"park");

	// Datatypes
	DatatypeProperty 
	latitudeProperty = ontModel.createDatatypeProperty(baseURI+"latitude"),
	longitudeProperty = ontModel.createDatatypeProperty(baseURI+"longitude"),
	idProperty = ontModel.createDatatypeProperty(baseURI+"id"),
	titleProperty = ontModel.createDatatypeProperty(baseURI+"title"),
	sizeProperty = ontModel.createDatatypeProperty(baseURI+"size"),
	priceProperty = ontModel.createDatatypeProperty(baseURI+"price"),
	pricepersqrmProperty = ontModel.createDatatypeProperty(baseURI+"pricepersqrm"),
	belowAverageProperty = ontModel.createDatatypeProperty(baseURI+"belowaverage"),
	addressProperty = ontModel.createDatatypeProperty(baseURI+"address"),
	linkProperty = ontModel.createDatatypeProperty(baseURI+"link");
	
	DatatypeProperty hasPark = ontModel.createDatatypeProperty(baseURI+"hasPark");
	hasPark.addDomain(parkClass);

	DatatypeProperty hasSchool = ontModel.createDatatypeProperty(baseURI+"hasSchool");
	hasPark.addDomain(schoolClass);
	
	// generate all Parks
	for(int i = 0 ; i<finalParks.size();i++){
		ParkEdited editedPark = finalParks.get(i);
		Individual parkIndividual = ontModel.createIndividual(baseURI+editedPark.getId(),parkClass);
		parkIndividual.addProperty(idProperty, editedPark.getId());
		parkIndividual.addProperty(titleProperty, editedPark.getName());
		parkIndividual.addProperty(latitudeProperty, editedPark.getLatitude().toString());
		parkIndividual.addProperty(longitudeProperty, editedPark.getLongitude().toString());
		
	}
	
	// generate all Schools
		for(int i = 0 ; i<finalSchools.size();i++){
			SchoolEdited editedSchool = finalSchools.get(i);
			Individual schoolIndividual = ontModel.createIndividual(baseURI+editedSchool.getId(),schoolClass);
			schoolIndividual.addProperty(idProperty, editedSchool.getId());
			schoolIndividual.addProperty(titleProperty, editedSchool.getName());
			schoolIndividual.addProperty(latitudeProperty, editedSchool.getLatitude());
			schoolIndividual.addProperty(longitudeProperty, editedSchool.getLongitude());
			schoolIndividual.addProperty(addressProperty, editedSchool.getAddress());

		}
	
	
	// generate Apartments
		for(int i = 0; i<finalApartments.size();i++){
			
		ApartmentEdited apartment = finalApartments.get(i);
		Individual apartmentIndividual = ontModel.createIndividual(baseURI+apartment.getId(),apartmentClass);
		
		
		apartmentIndividual.addProperty(idProperty, apartment.getId());
		apartmentIndividual.addProperty(priceProperty, apartment.getPrice());
		apartmentIndividual.addProperty(pricepersqrmProperty, apartment.getPriceSqrm());
		apartmentIndividual.addProperty(sizeProperty, apartment.getSize());
		apartmentIndividual.addProperty(belowAverageProperty, apartment.getPriceBelowAverage());
		apartmentIndividual.addProperty(latitudeProperty, apartment.getLatitude());
		apartmentIndividual.addProperty(longitudeProperty, apartment.getLongitude());
		apartmentIndividual.addProperty(linkProperty, apartment.getLink());



		ArrayList<DistanceObject> parks = apartment.getListOfParks();	
		ArrayList<DistanceObject> schools = apartment.getListOfSchools();	
			
		System.out.println("");

		System.out.println("Apartment "+apartment.getId());
		System.out.println("Anzahl Parks "+parks.size());
		System.out.println("Anzahl Schools "+schools.size());

			// ad parks within 500m
			
		for(int k = 0; k< parks.size();k++){
		
			apartmentIndividual.addProperty(hasPark, ontModel.getIndividual(baseURI+parks.get(k).getId()));
			System.out.println("hasPark "+parks.get(k).getId());
		}
			
			
		for(int k = 0; k< schools.size();k++){
			
			apartmentIndividual.addProperty(hasSchool, ontModel.getIndividual(baseURI+schools.get(k).getId()));
			System.out.println("hasSchool "+schools.get(k).getId());

		}
			
			
			
			
			
		ontModel.setNsPrefix("ap", baseURI);
		 //ontModel.write(System.out, "RDF/XML");

		 ontModel.write(new BufferedWriter(new OutputStreamWriter(new FileOutputStream("apartment-model"), "UTF8")));
			
			
			
			
			
		}
	
		
		
		
		

}
}